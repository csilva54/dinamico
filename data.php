<?php

    require_once('config.php');

    // trampando com datas

    // string date(string $format[, int $timestamp = time()]);
    echo date('d-m-Y').'<br>';
    echo date('H:i:s v').'<br>';
    echo date_default_timezone_get().'<br>';

    $timestamp = time();

    echo($timestamp).'<br>';
    echo date('d-m-Y H:i:s', $timestamp).'<br>';
    
    $timestamp = mktime(19,49,00,04,29,2000);
    echo $timestamp.'<br>';
    
    // recebe data e hora
    $data = '09-10-1999 18:25:05';
    // Converte data e hora em timestamp
    $timestamp1 = strtotime($data);
    echo $timestamp1.'<br>';
    echo date('d/m/Y H:i:s', $timestamp1).'<br>';


    $timestamp2 = strtotime($data);
    echo $timestamp2.'<br>';
    echo date('d/m/Y H:i:s', $timestamp2).'<br>';

    $data_padrao = new Datetime();
    // print_r($data_padrao).'<br>';

    $data_fin = new Datetime('17-09-2019 15:32:09');
    $dia_am = new Datetime('+1 day');

    echo $dia_fin->format('d m Y');
?>


