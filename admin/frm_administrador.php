<?php
    require_once('../config.php')
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Fomulário Admin</title>
</head>
<body>
  <form action="op_administrador.php" method='post'>
    <fieldset>        
        <input type="hidden" id="id" name="id">
        <label for="">Nome</label>
        <input type="text" name="nome" required placeholder="Digite o nome">
        <br>
        <label for="">Email</label>
        <input type="email" name="email" required placeholder="Digite o e-mail">
        <br>
        <label for="">Login</label>
        <input type="text" name="login" required placeholder="Digite o login">
        <br>
        <label for="">Senha</label>
        <input type="password" name="senha" required placeholder="Digite o senha">
        <br>
        <label for="">Confirme a senha</label>
        <input type="password" name="confirme_senha" required placeholder="Digite novamente a senha">
        <hr>
        <input type="submit" value="Cadastrar" name="cadastro"> 
    </fieldset>
  </form>
</body>
</html>