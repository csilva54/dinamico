<?php
    require_once('conexao.php');
    // verificar se o usuario clicou no botão cadastrar
    if(isset($_POST['cadastro'])){
        // recuperar dados dos campos do formulario
        $nome = $_POST['nome'];
        $email = $_POST['email'];
        $foto = $_FILES['foto'];
        
        if(!empty($foto['name'])){
            // largura máxima da foto em pixels
            $largura = 1200;            
            // altura máxima da foto em pixels
            $altura = 1200;            
            // Tamanho máximo
            $tamanho = 1000000;

            $error = array();
            // Verifica se é uma imagem
            if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/",$foto['type'])){
                $error[1]="Isso não é uma imagem";
            }
            // recuperando as dimensões
            $dimensoes = getimagesize($foto['tmp_name']);
            // verificar a largura da imagem é maior que a permitida
            if($dimensoes[0]>$largura){
                $error[2]="A largura da imagem é maior do que a permitida. Não deve ultrapassar ".$largura." pixels.";
            }
            // verificar a altura da imagem é maior que a permitida
            if($dimensoes[1]>$altura){
                $error[3]="A altura da imagem é maior do que a permitida";
            }
            // verificar a tamanho da imagem é maior que a permitida
            if($foto['size']>$tamanho){
                $error[4]="O tamanho da imagem é maior do que o permitido";
            }
            // se não houver erros
            if(count($error)==0){
                // recupera a extensão do arquivo de imagem
                preg_match("/\.(gif|bmp|png|jpg){1}$/i",$foto['name'], $ext);
                // gera um nome de arquivo único de imagem                
                $nome_imagem = md5(uniqid(time())).".".$ext[1];
                // caminho para armazenar a imagem
                $caminho_imagem = "foto/".$nome_imagem;
                // realiza o upload da imagem a partir do espaço temporário
                move_uploaded_file($foto['tmp_name'], $caminho_imagem);

                //Gravar no banco de dados
                $cmd = $conn->prepare("INSERT INTO usuario (nome, email, foto) VALUES (:nome, :email, :foto)");
                $result = $cmd->execute(array(
                    ":nome"=>$nome,
                    ":email"=>$email,
                    ":foto"=>$nome_imagem
                ));
                
                if($result){
                    echo "Usuário cadastrado com sucesso.";                             
                }
            }           
        }
        if(count($error) != 0){
            foreach ($error as $erro) {
                echo $erro."<br>";
            }
        }
    }

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cadastro de usuário</title>
</head>
<body>
    <h1>Novo usuário</h1>
    <form action="<?php echo $_SERVER['PHP_SELF']?>" method="post" enctype="multipart/form-data" name="cadastro_form">
        Nome:<br>
        <input type="text" name="nome" required><br><br>
        Email:<br>
        <input type="email" name="email" required><br><br>
        Foto de exibição:<br>
        <input type="file" name="foto"><br><br>        
        <input type="submit" name="cadastro" value="Cadastrar">
    </form>    
    <br>
    <hr>
    <h2>Usuários cadastrados</h2>
    <table BORDER=1px>
    <TR> 
	    <TH>Foto</TH>
	    <TH>Nome </TH>
	    <TH>Email </TH>
    </TR>   
    <?php
        $cmd = $conn->prepare("SELECT * FROM usuario ORDER BY id");
        $fiz = $cmd->execute();
        $result = $cmd->fetchAll(PDO::FETCH_ASSOC);
        
        if($fiz){
            foreach($result as $usuario){
                echo "<tr>";
                    echo "<TH><img src='foto/".$usuario['foto']."' width='56px' height='48'></td>";
                    echo"<td>".$usuario['nome']."</td>";
                    echo"<td>".$usuario['email']."</td>";
                echo "</tr>";
            }
        }        
    ?>
    </table>
</body>
</html>