<?php
    // require_once('../config.php')
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    <title>Fomulário Admin</title>
</head>
<body>
  <form action="op_noticia.php" method='post'>
    <fieldset>        
        <input type="hidden" id="idcategoria" name="idcategoria">
        
        <label for="">Categoria</label>  
            <?php
             require_once("../config.php");
             $categoria = Categoria::getList();
            ?>
            <select name="categoria" id="categoria">
                <option value=""></option>
                <?php
                    foreach ($categoria as $cat){    
                ?>
                <option value="<?php echo $cat['id_categoria'].'">'.$cat['id_categoria']."-".$cat['categoria'];?></option>
                <?php }?>
            </select>                   
        <br>
        <label for="">Titulo noticia</label>
        <input type="text" name="titulo_noticia" required>
        <br>        
        <label for="">Visita noticia</label>
        <input type="text" name="visita_noticia" required>
        <br>
        <label for="">Data noticia</label>        
        <input type="date" name="data_noticia" required>
        <br>
        <label for="">Ativo</label>        
        <input type="text" name="noticia_ativo" required>
        <br>
        <label for="">Noticia</label>        
        <input type="text" name="noticia" required>
        <br>
        <label for="">Imagem</label>
        <!-- <input type="file" name="img_noticia" required> -->
        <input type="text" name="img_noticia" required>
        <br>
        <hr>
        <input type="submit" value="Cadastrar" name="cadastro"> 
    </fieldset>
  </form>
</body>
</html>