<?php
// require_once('admin/conexao.php');
// inicializar a sessão de usuário
// session_start();

// Definindo padrão de zona GMT (TimeZona) -3,00
date_default_timezone_set('America/Sao_Paulo');

// inicializar variaveis de banco de dados

// inicia carregamento das classes do projeto
spl_autoload_register(function($nome_classe){
    $nome_arquivo = "class".DIRECTORY_SEPARATOR.$nome_classe.".php";
    if(file_exists($nome_arquivo)){
        require_once($nome_arquivo);
    }
});


// criando uma constante
define('IP_SERVER_DB','127.0.0.1');
define('HOSTNAME','ITQ0626029W10-1');
define('BANCO','dinamicodb');
define('USER_DB','root');
define('PASS_DB','');

?>